import { Component, OnInit } from '@angular/core';
import { TodoListRepository } from '../../Repositories/TodoListRepository';
import { AccountRepository } from '../../Repositories/AccountRepository';
import { StorageService } from '../../Services/StorageService';
import { Router } from '@angular/router';

@Component({
    selector: 'todoList',
    templateUrl: './TodoListComponent.html',
    styleUrls: ['./TodoListComponent.css']
})
export class TodoListComponent implements OnInit {
    user = {};
    TodoList = [];
    itemText = '';
    constructor(
        private todoListRepository: TodoListRepository,
        private accountRepository: AccountRepository,
        private storageService: StorageService,
        private router: Router
    ) { }

    ngOnInit() {
        this.user = this.storageService.GetCurrentUser();
        this.todoListRepository.GetUserTodoList(this.user["_id"]).subscribe(
            res => {
                this.TodoList = res["Data"];
                console.log(this.TodoList);
            },
            err => {
                console.log(err);
            }
        )
    }

    addItem() {
        let item = {
            userId: this.user["_id"],
            text: this.itemText,
            createdAt: new Date(),
            done: false
        };
        this.todoListRepository.CreateTodo(item).subscribe(
            res => {
                if (res["Success"]) {
                    this.TodoList.push(res["Data"]);
                    this.itemText = '';
                } else {
                    console.log(res["Message"]);
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    deleteItem(row) {
        console.log(row);
        this.todoListRepository.deleteItem(row._id).subscribe(
            res => {
                if (res["Success"]) {
                    console.log(res);
                    let itemIndex = this.TodoList.indexOf(row);
                    this.TodoList.splice(itemIndex, 1);
                } else {
                    console.log(res["Message"]);
                }
                console.log(res);
            },
            err => {
                console.log(err)
            }
        )
    }

    doneItem(row) {
        this.todoListRepository.doneItem(row._id).subscribe(
            res => {
                if (res["Success"]) {
                    console.log(res);
                    row.done = true;
                    let itemIndex = this.TodoList.indexOf(row);
                    this.TodoList.splice(itemIndex, row);
                } else {
                    console.log(res["Message"]);
                }
                console.log(res);
            },
            err => {
                console.log(err)
            }
        )
    }

    logout() {
        this.storageService.RemoveCurrentUser();
        this.router.navigateByUrl('/login');
    }
}