import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AccountRepository } from '../../Repositories/AccountRepository';

@Component({
    selector: 'signup',
    templateUrl: './RegisterComponent.html'
})
export class SignupComponent {
    errorMessage = '';
    isErrorMessage: any;

    user = {
        email: '',
        username: '',
        password:''
    }

    constructor(
        private accountRepository: AccountRepository,
        private router: Router
        ) { }

    Register(frm: any) {
        if (frm.valid) {
            this.accountRepository.Register(this.user).subscribe(
                response => {
                    if (response["Success"] === true) {
                        this.isErrorMessage = 3;
                        this.errorMessage = 'You have Registered Successfully';
                        this.router.navigateByUrl('/login');
                    } else if (response["Message"] === 'USER_HAS_REGISTERED_BEFORE') {
                        this.isErrorMessage = 2;
                        this.errorMessage = 'User has registered before';
                    } else {
                        this.isErrorMessage = 2;
                        this.errorMessage = 'Operation Failed';
                    }
                },
                err => {
                    this.isErrorMessage = 2;
                    this.errorMessage = 'Operation Failed';
                }
            )
        }
    }

}
