import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './Routes';

import { LocalStorageModule } from 'angular-2-local-storage';

import { AppComponent } from './app.component';
import { LoginComponent } from './Features/Login/LoginComponent';
import { SignupComponent } from './Features/Register/RegisterComponent';
import { TodoListComponent } from './Features/TodoList/TodoListComponent';
import { AccountRepository } from './Repositories/AccountRepository';
import { TodoListRepository } from './Repositories/TodoListRepository';

import { StorageService } from './Services/StorageService';
import { AuthGuard } from './Services/AuthGuardService';
import { TokenInterceptor } from './Services/Interceptor';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    TodoListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    LocalStorageModule.forRoot({
      prefix: 'my-app',
      storageType: 'localStorage'
    })

  ],
  providers: [AccountRepository,
    TodoListRepository,
    StorageService,
    AuthGuard
    // ,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: TokenInterceptor,
    //   multi: true
    // }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
