import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class StorageService {

    constructor(private localStorageService: LocalStorageService) { }

    SetCurrentUser(currentUser) {
        this.localStorageService.set('currentUser', currentUser);
    }

    GetCurrentUser() {
        return this.localStorageService.get('currentUser');
    }

    RemoveCurrentUser() {
        return this.localStorageService.remove('currentUser');
    }

}

