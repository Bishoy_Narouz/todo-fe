import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TodoListRepository {

    constructor(
        private http: HttpClient,
    ) { }

    hostUrl: string = 'http://localhost:5000/api/';
    apiUrl: string = this.hostUrl + 'Todo/';

    GetUserTodoList(userId) {
        let url = this.apiUrl + "GetUserTodoList?userId=";
        return this.http.get(url + userId);
    }

    CreateTodo(model) {
        let url = this.apiUrl + "createTodo";
        return this.http.post(url, model);
    }

    deleteItem(itemId) {
        let url = this.apiUrl + "deleteItem?_id=" + itemId;
        return this.http.delete(url);
    }

    doneItem(_id) {
        let url = this.apiUrl + "doneItem";
        return this.http.put(url, { _id: _id });
    }

}
