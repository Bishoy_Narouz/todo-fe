import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AccountRepository {

    constructor(
        private http: HttpClient,
    ) { }

    hostUrl: string = 'http://localhost:5000/api/';
    apiUrl: string = this.hostUrl + 'Account/';

    Login(model) {
        let url = this.apiUrl + "Login";
        return this.http.post(url, model);
    }

    Register(model) {
        let url = this.apiUrl + "Register";
        return this.http.post(url, model);
    }

}
